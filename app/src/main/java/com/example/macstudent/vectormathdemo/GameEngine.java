package com.example.macstudent.vectormathdemo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "VECTOR-MATH";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    private Sprite bullet;
    private Sprite square;
    final int SIZE = 100;

    boolean isMovingDown = false;
    final int SQUARESPEED = 12;
    final int numberBullets = 10;

    List<Sprite> bullets = new ArrayList<Sprite>();

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        //bullet = new Sprite(getContext(), 100, 600, SIZE, SIZE);
        for (int i = 0; i < numberBullets; i++){
           Random r = new Random();
           int randomX = r.nextInt(this.screenWidth - 100) + 1;
           int randomY = r.nextInt(this.screenHeight - 300) + 1;
           Sprite b = new Sprite(getContext(), randomX/*100+(i*100)*/, randomY/*600+(i*100)*/, SIZE, SIZE);
           bullets.add(b);
        }

        Random r = new Random();
        int randomX = r.nextInt(this.screenWidth - 100) + 1;
        int randomY = r.nextInt(this.screenHeight - 300) + 1;
        //square = new Sprite(getContext(), 1000, 100, SIZE, SIZE);
        square = new Sprite(getContext(), randomX, randomY, SIZE, SIZE);

    }

    @Override
    public void run() {
        // @TODO: Put game loop in here
        while (gameIsRunning == true) {
            updateGame();
            drawGame();
            controlFPS();
        }
    }


    // Game Loop methods
    public void updateGame() {

        //move square
        /*Random r = new Random();
        int randomX = r.nextInt(this.screenWidth - 100) + 1;
        int randomY = r.nextInt(this.screenHeight - 300) + 1;
        square.x = randomX;
        square.y = randomY;

        if (isMovingDown == true) {
            square.y = square.y + SQUARESPEED;

            if (square.y >= this.screenHeight-260){
                isMovingDown = false;
            }
        }else{
            square.y = square.y - SQUARESPEED;

            if (square.y <= 0){
                isMovingDown = true;
            }
        }*/


        for (int i = 0; i < bullets.size(); i++){
            Sprite help = bullets.get(i);

            //move bullet
            //@TODO: discover distance
            double x, y, distance;
            x = (square.x - help.x);
            y = (square.y - help.y);
            distance = Math.pow(x,2) + Math.pow(y,2);
            distance = Math.sqrt(distance);

            //@TODO: normalization
            double xn, yn;
            xn = x / distance;
            yn = y / distance;

            help.x = help.x + (int) (xn * 9);
            help.y = help.y + (int) (yn * 9);
        }

    }


    public void drawGame() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------
            // @TODO: put your drawing code in this section

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setStrokeWidth(10);
            paintbrush.setColor(Color.MAGENTA);
            canvas.drawRect(square.x, square.y, square.x+SIZE, square.y+SIZE, paintbrush);

            paintbrush.setColor(Color.BLACK);
            for (int i = 0; i < bullets.size(); i++) {
                canvas.drawRect(bullets.get(i).x, bullets.get(i).y, bullets.get(i).x + SIZE, bullets.get(i).y + SIZE, paintbrush);
            }


            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                square.x = (int) event.getX();
                square.y = (int) event.getY();

                break;
            case MotionEvent.ACTION_DOWN:

                break;
        }
        return true;
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}
